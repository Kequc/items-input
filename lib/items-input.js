var ItemsInput = (function () {
    function ItemsInput(element) {
        var _this = this;
        this._isHovering = false;
        // data
        this.items = [];
        this.suggestions = [];
        // find important elements
        this.element = element;
        this.parts = {
            items: this.element.querySelector('.items'),
            input: this.element.querySelector('.items input'),
            fields: this.element.querySelector('.fields')
        };
        // prepopulate
        if (this.element.dataset['values']) {
            var values = this.element.dataset['values'].split(',');
            for (var _i = 0, values_1 = values; _i < values_1.length; _i++) {
                var value = values_1[_i];
                this.addItem(value);
            }
        }
        // div behaves similar to input
        this.parts.items.addEventListener('click', function () { _this.parts.input.select(); });
        // prevent default behaviour on real input
        this.parts.input.addEventListener('click', function (e) { e.stopPropagation(); });
        this.parts.input.addEventListener('keyup', this._preventSubmit.bind(this));
        this.parts.input.addEventListener('keypress', this._preventSubmit.bind(this));
        // check input value
        this.parts.input.addEventListener('keyup', this._onKeyup.bind(this));
        this.parts.input.addEventListener('focus', this.checkValue.bind(this));
        this.parts.input.addEventListener('blur', this._onBlur.bind(this));
    }
    ItemsInput.prototype._preventSubmit = function (e) {
        if ([13, 169].indexOf(e.which || e.charCode || e.keyCode) > -1) {
            // enter key pressed
            e.preventDefault();
            this.addItem(this.parts.input.value);
            this.parts.input.value = "";
            this.parts.input.focus();
            this.unsuggest();
        }
    };
    ItemsInput.prototype._onBlur = function () {
        if (!this._isHovering) {
            // removed focus
            if (this.parts.input.value) {
                this.addItem(this.parts.input.value);
                this.parts.input.value = "";
            }
            this.unsuggest();
        }
    };
    ItemsInput.prototype._onKeyup = function (e) {
        if ([13, 169, 37, 38, 39, 40].indexOf(e.which || e.charCode || e.keyCode) <= -1) {
            // non-arrow non-enter key pressed
            this.checkValue();
        }
    };
    ItemsInput.prototype.checkValue = function () {
        // see if there are suggestions
        if (!this.parts.input.value)
            this.unsuggest();
        else
            this.suggest(this.parts.input.value);
    };
    ItemsInput.prototype.suggest = function (value) {
        if (!this.suggestions.length)
            return;
        // suggest possible matches
        this._populateSuggestionsElement(value);
        this.parts.suggestions.classList.remove('is-hidden');
    };
    ItemsInput.prototype.unsuggest = function () {
        // hide
        if (this.parts.suggestions)
            this.parts.suggestions.classList.add('is-hidden');
    };
    ItemsInput.prototype._populateSuggestionsElement = function (value) {
        this._buildSuggestionsElement();
        // clear list
        while (this.parts.suggestions.firstChild) {
            this.parts.suggestions.removeChild(this.parts.suggestions.firstChild);
        }
        // find suggestions for value
        var results = [];
        for (var index = 0; index < this.suggestions.length; index++) {
            if (this.items.indexOf(index) <= -1) {
                // not already added
                var suggestion = this.suggestions[index];
                // match
                if (suggestion.value.toLowerCase().indexOf(value.toLowerCase()) > -1)
                    results.push(suggestion);
            }
        }
        // common or alphabetical order
        results.sort(function (a, b) {
            var alphabetical = ((a.value < b.value) ? -1 : ((a.value > b.value) ? 1 : 0));
            return ((a.count > b.count) ? -1 : ((a.count < b.count) ? 1 : alphabetical));
        });
        // populate suggestions element
        for (var _i = 0, results_1 = results; _i < results_1.length; _i++) {
            var suggestion = results_1[_i];
            this.parts.suggestions.appendChild(this._getSuggestionElement(suggestion));
        }
    };
    ItemsInput.prototype._buildSuggestionsElement = function () {
        var _this = this;
        if (this.parts.suggestions)
            return;
        // create suggestions element
        this.parts.suggestions = document.createElement('div');
        this.parts.suggestions.classList.add('suggestions', 'is-hidden');
        this.parts.suggestions.addEventListener('mouseenter', function () { _this._isHovering = true; });
        this.parts.suggestions.addEventListener('mouseleave', function () {
            _this._isHovering = false;
            if (_this.parts.input != document.activeElement)
                _this._onBlur();
        });
        this.element.appendChild(this.parts.suggestions);
    };
    ItemsInput.prototype._getSuggestionElement = function (suggestion) {
        var _this = this;
        if (!suggestion.element) {
            // create suggestion element
            var element = document.createElement('div');
            element.classList.add('suggestion');
            element.appendChild(document.createTextNode(suggestion.value));
            element.addEventListener('click', function () {
                _this.addItem(suggestion.value);
                _this.parts.input.value = "";
                _this.parts.input.focus();
                _this.unsuggest();
            });
            suggestion.element = element;
        }
        return suggestion.element;
    };
    ItemsInput.prototype.addItem = function (value) {
        if (!value)
            return;
        // add the item
        var index = this._getSuggestionIndex(value);
        if (this.items.indexOf(index) <= -1) {
            // new item
            this._addItemElements(index);
            this.items.push(index);
        }
    };
    ItemsInput.prototype._addItemElements = function (index) {
        var _this = this;
        var value = this.suggestions[index].value;
        // physically add item to form
        var item = document.createElement('div');
        item.classList.add('item');
        item.appendChild(document.createTextNode(value));
        item.addEventListener('click', function () {
            // ability to remove again
            _this.removeItem(value);
            _this.parts.input.select();
        });
        this.parts.items.insertBefore(item, this.parts.input);
        // hidden input too
        var field = document.createElement('input');
        field.setAttribute('type', 'hidden');
        field.setAttribute('name', (this.element.dataset['name'] || "items") + "[]");
        field.setAttribute('value', value);
        this.parts.fields.appendChild(field);
    };
    ItemsInput.prototype.removeItem = function (value) {
        if (!value)
            return;
        // physically remove item from form
        var index = this.items.indexOf(this._getSuggestionIndex(value));
        if (index > -1) {
            // found
            this.parts.items.removeChild(this.parts.items.children.item(index));
            this.parts.fields.removeChild(this.parts.fields.children.item(index));
            this.items.splice(index, 1);
        }
    };
    ItemsInput.prototype._addSuggestion = function (value, count) {
        // update count if exists
        for (var _i = 0, _a = this.suggestions; _i < _a.length; _i++) {
            var suggestion = _a[_i];
            if (suggestion.value.toLowerCase() == value.toLowerCase()) {
                // exists
                suggestion.count = count;
                return;
            }
        }
        // doesn't exist
        this.suggestions.push({ value: value, count: count });
    };
    ItemsInput.prototype.addSuggestions = function (suggestions) {
        if (suggestions === void 0) { suggestions = []; }
        for (var _i = 0, suggestions_1 = suggestions; _i < suggestions_1.length; _i++) {
            var suggestion = suggestions_1[_i];
            this._addSuggestion(suggestion.value, suggestion.count);
        }
    };
    ItemsInput.prototype._getSuggestionIndex = function (value) {
        // find suggestion by value
        for (var index = 0; index < this.suggestions.length; index++) {
            if (this.suggestions[index].value.toLowerCase() == value.toLowerCase()) {
                // found
                return index;
            }
        }
        // doesn't exist add one
        this.suggestions.push({ value: value, count: 0 });
        return this.suggestions.length - 1;
    };
    return ItemsInput;
}());
document.addEventListener('DOMContentLoaded', function () {
    // relevant elements
    var instance = new ItemsInput(document.getElementById('repo-demo'));
    var suggestions = [{ "value": "alligator", "count": 1 }, { "value": "ant", "count": 1 }, { "value": "bear", "count": 1 }, { "value": "bee", "count": 1 }, { "value": "bird", "count": 1 }, { "value": "camel", "count": 1 }, { "value": "cat", "count": 1 }, { "value": "cheetah", "count": 1 }, { "value": "chicken", "count": 1 }, { "value": "chimpanzee", "count": 1 }, { "value": "cow", "count": 1 }, { "value": "crocodile", "count": 1 }, { "value": "deer", "count": 1 }, { "value": "dog", "count": 1 }, { "value": "dolphin", "count": 1 }, { "value": "duck", "count": 1 }, { "value": "eagle", "count": 1 }, { "value": "elephant", "count": 1 }, { "value": "fish", "count": 1 }, { "value": "fly", "count": 1 }, { "value": "fox", "count": 1 }, { "value": "frog", "count": 1 }, { "value": "giraffe", "count": 1 }, { "value": "goat", "count": 1 }, { "value": "goldfish", "count": 1 }, { "value": "hamster", "count": 1 }, { "value": "hippopotamus", "count": 1 }, { "value": "horse", "count": 1 }, { "value": "kangaroo", "count": 1 }, { "value": "kitten", "count": 1 }, { "value": "lion", "count": 1 }, { "value": "lobster", "count": 1 }, { "value": "monkey", "count": 1 }, { "value": "octopus", "count": 1 }, { "value": "owl", "count": 1 }, { "value": "panda", "count": 1 }, { "value": "pig", "count": 1 }, { "value": "puppy", "count": 1 }, { "value": "rabbit", "count": 1 }, { "value": "rat", "count": 1 }, { "value": "scorpion", "count": 1 }, { "value": "seal", "count": 1 }, { "value": "shark", "count": 1 }, { "value": "sheep", "count": 1 }, { "value": "snail", "count": 1 }, { "value": "snake", "count": 1 }, { "value": "spider", "count": 1 }, { "value": "squirrel", "count": 1 }, { "value": "tiger", "count": 1 }, { "value": "turtle", "count": 1 }, { "value": "wolf", "count": 1 }, { "value": "zebra", "count": 1 }];
    var random = Math.floor(Math.random() * (suggestions.length - 1));
    instance.addItem(suggestions[random].value);
    instance.addSuggestions(suggestions);
});
