interface ISuggestion {
    value: string;
    count: number;
    element?: HTMLElement;
}

class ItemsInput {

    private _isHovering: boolean = false;

    // ui elements
    element: HTMLElement;
    parts: {
        items: HTMLElement,
        input: HTMLInputElement,
        fields: HTMLElement;
        suggestions?: HTMLElement
    };

    // data
    items: number[] = [];
    suggestions: ISuggestion[] = [];

    constructor (element: HTMLElement)
    {
        // find important elements
        this.element = element;
        this.parts = {
            items: <HTMLElement>this.element.querySelector('.items'),
            input: <HTMLInputElement>this.element.querySelector('.items input'),
            fields: <HTMLElement>this.element.querySelector('.fields')
        };

        // prepopulate
        if (this.element.dataset['values']) {
            let values = this.element.dataset['values'].split(',');
            for (let value of values) {
                this.addItem(value);
            }
        }

        // div behaves similar to input
        this.parts.items.addEventListener('click', () => { this.parts.input.select(); });
        // prevent default behaviour on real input
        this.parts.input.addEventListener('click', (e: Event) => { e.stopPropagation(); });
        this.parts.input.addEventListener('keyup', this._preventSubmit.bind(this));
        this.parts.input.addEventListener('keypress', this._preventSubmit.bind(this));
        // check input value
        this.parts.input.addEventListener('keyup', this._onKeyup.bind(this));
        this.parts.input.addEventListener('focus', this.checkValue.bind(this));
        this.parts.input.addEventListener('blur', this._onBlur.bind(this));
    }

    private _preventSubmit (e: KeyboardEvent)
    {
        if ([13,169].indexOf(e.which || e.charCode || e.keyCode) > -1) {
            // enter key pressed
            e.preventDefault();
            this.addItem(this.parts.input.value);
            this.parts.input.value = "";
            this.parts.input.focus();
            this.unsuggest();
        }
    }

    private _onBlur ()
    {
        if (!this._isHovering) {
            // removed focus
            if (this.parts.input.value) {
                this.addItem(this.parts.input.value);
                this.parts.input.value = "";
            }
            this.unsuggest();
        }
    }

    private _onKeyup (e: KeyboardEvent)
    {
        if ([13,169,37,38,39,40].indexOf(e.which || e.charCode || e.keyCode) <= -1) {
            // non-arrow non-enter key pressed
            this.checkValue();
        }
    }

    checkValue ()
    {
        // see if there are suggestions
        if (!this.parts.input.value)
            this.unsuggest();
        else
            this.suggest(this.parts.input.value);
    }

    suggest (value: string)
    {
        if (!this.suggestions.length)
            return;
        // suggest possible matches
        this._populateSuggestionsElement(value);
        this.parts.suggestions.classList.remove('is-hidden');
    }

    unsuggest ()
    {
        // hide
        if (this.parts.suggestions)
            this.parts.suggestions.classList.add('is-hidden');
    }

    private _populateSuggestionsElement (value: string)
    {
        this._buildSuggestionsElement();
        // clear list
        while (this.parts.suggestions.firstChild) {
            this.parts.suggestions.removeChild(this.parts.suggestions.firstChild);
        }

        // find suggestions for value
        let results: ISuggestion[] = [];
        for (let index = 0; index < this.suggestions.length; index++) {
            if (this.items.indexOf(index) <= -1) {
                // not already added
                let suggestion = this.suggestions[index];
                // match
                if (suggestion.value.toLowerCase().indexOf(value.toLowerCase()) > -1)
                    results.push(suggestion);
            }
        }

        // common or alphabetical order
        results.sort((a, b) => {
            let alphabetical = ((a.value < b.value) ? -1 : ((a.value > b.value) ? 1 : 0));
            return ((a.count > b.count) ? -1 : ((a.count < b.count) ? 1 : alphabetical));
        });

        // populate suggestions element
        for (let suggestion of results) {
            this.parts.suggestions.appendChild(this._getSuggestionElement(suggestion));
        }
    }

    private _buildSuggestionsElement ()
    {
        if (this.parts.suggestions)
            return;
        // create suggestions element
        this.parts.suggestions = document.createElement('div');
        this.parts.suggestions.classList.add('suggestions', 'is-hidden');

        this.parts.suggestions.addEventListener('mouseenter', () => { this._isHovering = true; });
        this.parts.suggestions.addEventListener('mouseleave', () => {
            this._isHovering = false;
            if (this.parts.input != document.activeElement)
                this._onBlur();
        });

        this.element.appendChild(this.parts.suggestions);
    }

    private _getSuggestionElement (suggestion: ISuggestion): HTMLElement
    {
        if (!suggestion.element) {
            // create suggestion element
            let element = document.createElement('div');
            element.classList.add('suggestion');
            element.appendChild(document.createTextNode(suggestion.value));
            element.addEventListener('click', () => {
                this.addItem(suggestion.value);
                this.parts.input.value = "";
                this.parts.input.focus();
                this.unsuggest();
            });
            suggestion.element = element;
        }
        return suggestion.element;
    }
    
    addItem (value: string)
    {
        if (!value)
            return;
        // add the item
        let index = this._getSuggestionIndex(value);
        if (this.items.indexOf(index) <= -1) {
            // new item
            this._addItemElements(index);
            this.items.push(index);
        }
    }

    private _addItemElements (index: number)
    {
        let value = this.suggestions[index].value;
        
        // physically add item to form
        let item = document.createElement('div');
        item.classList.add('item');
        item.appendChild(document.createTextNode(value));
        item.addEventListener('click', () => {
            // ability to remove again
            this.removeItem(value);
            this.parts.input.select();
        });
        this.parts.items.insertBefore(item, this.parts.input);

        // hidden input too
        let field = document.createElement('input');
        field.setAttribute('type', 'hidden');
        field.setAttribute('name', (this.element.dataset['name'] || "items") + "[]");
        field.setAttribute('value', value);
        this.parts.fields.appendChild(field);
    }

    removeItem (value: string)
    {
        if (!value)
            return;
        // physically remove item from form
        let index = this.items.indexOf(this._getSuggestionIndex(value)); 
        if (index > -1) {
            // found
            this.parts.items.removeChild(this.parts.items.children.item(index));
            this.parts.fields.removeChild(this.parts.fields.children.item(index));
            this.items.splice(index, 1);
        }
    }

    private _addSuggestion (value: string, count: number)
    {
        // update count if exists
        for (let suggestion of this.suggestions) {
            if (suggestion.value.toLowerCase() == value.toLowerCase()) {
                // exists
                suggestion.count = count;
                return;
            }
        }
        // doesn't exist
        this.suggestions.push({ value: value, count: count });
    }

    addSuggestions (suggestions: { value: string, count: number }[] = [])
    {
        for (let suggestion of suggestions) {
            this._addSuggestion(suggestion.value, suggestion.count);
        }
    }

    private _getSuggestionIndex (value: string): number
    {
        // find suggestion by value
        for (let index = 0; index < this.suggestions.length; index++) {
            if (this.suggestions[index].value.toLowerCase() == value.toLowerCase()) {
                // found
                return index;
            }
        }
        // doesn't exist add one
        this.suggestions.push({ value: value, count: 0 });
        return this.suggestions.length - 1;
    }
}

document.addEventListener('DOMContentLoaded', () => {

    // relevant elements
    let instance = new ItemsInput(document.getElementById('repo-demo'));

    let suggestions = [{ "value": "alligator", "count": 1 }, { "value": "ant", "count": 1 }, { "value": "bear", "count": 1 }, { "value": "bee", "count": 1 }, { "value": "bird", "count": 1 }, { "value": "camel", "count": 1 }, { "value": "cat", "count": 1 }, { "value": "cheetah", "count": 1 }, { "value": "chicken", "count": 1 }, { "value": "chimpanzee", "count": 1 }, { "value": "cow", "count": 1 }, { "value": "crocodile", "count": 1 }, { "value": "deer", "count": 1 }, { "value": "dog", "count": 1 }, { "value": "dolphin", "count": 1 }, { "value": "duck", "count": 1 }, { "value": "eagle", "count": 1 }, { "value": "elephant", "count": 1 }, { "value": "fish", "count": 1 }, { "value": "fly", "count": 1 }, { "value": "fox", "count": 1 }, { "value": "frog", "count": 1 }, { "value": "giraffe", "count": 1 }, { "value": "goat", "count": 1 }, { "value": "goldfish", "count": 1 }, { "value": "hamster", "count": 1 }, { "value": "hippopotamus", "count": 1 }, { "value": "horse", "count": 1 }, { "value": "kangaroo", "count": 1 }, { "value": "kitten", "count": 1 }, { "value": "lion", "count": 1 }, { "value": "lobster", "count": 1 }, { "value": "monkey", "count": 1 }, { "value": "octopus", "count": 1 }, { "value": "owl", "count": 1 }, { "value": "panda", "count": 1 }, { "value": "pig", "count": 1 }, { "value": "puppy", "count": 1 }, { "value": "rabbit", "count": 1 }, { "value": "rat", "count": 1 }, { "value": "scorpion", "count": 1 }, { "value": "seal", "count": 1 }, { "value": "shark", "count": 1 }, { "value": "sheep", "count": 1 }, { "value": "snail", "count": 1 }, { "value": "snake", "count": 1 }, { "value": "spider", "count": 1 }, { "value": "squirrel", "count": 1 }, { "value": "tiger", "count": 1 }, { "value": "turtle", "count": 1 }, { "value": "wolf", "count": 1 }, { "value": "zebra", "count": 1 }];

    let random = Math.floor(Math.random() * (suggestions.length - 1));
    instance.addItem(suggestions[random].value);
    instance.addSuggestions(suggestions);
    
});
